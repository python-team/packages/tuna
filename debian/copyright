Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: tuna
Source: https://git.kernel.org/pub/scm/utils/tuna/tuna.git/

Files: *
Copyright: 2008,2009,2010,2011 Red Hat Inc. <jkastner@redhat.com>
License: GPL-2
Comment: Content was authored by Arnaldo Carvalho de Melo <acme@redhat.com> in
 2008. Maintainership transferred to Jiri Kastner <jkastner@redhat.com> in
 2015. Jiri Kasterner (as of 2018) is the point of contact for all upstream
 matters related to this library. Copyright claim and license derived from
 tuna-cmd.py, tuna/__init__.py, tuna/gui/__init__.py which serve as the main
 entry-points for the application and supporting libraries.

Files: debian/*
Copyright: 2018 Stewart Ferguson <stew@ferg.aero>
License: GPL-2

Files: docs/oscilloscope+tuna.html
Copyright: 1996-2006 Kasper Skaarhoj
License: GPL-2
Comment: Copyright claim limited to framework used to generate file. The
 file contents are governed by the parent claim defined in this document.
 .
 OSADL - The Open Source Automation Development Lab
 .
 This website is powered by TYPO3 - inspiring people to share!
 TYPO3 is a free open source Content Management Framework initially created by Kasper Skaarhoj and licensed under GNU/GPL.
 TYPO3 is copyright 1998-2006 of Kasper Skaarhoj. Extensions are copyright of their respective owners.
 Information and contribution at http://typo3.com/ and http://typo3.org/

Files:
 oscilloscope-cmd.py
 tuna/oscilloscope.py
Copyright: 2008-2009 Red Hat, Inc. <acme@redhat.com>
License: LGPL-2.1
Comment: Copyright holder and datedefined in tuna/oscilloscope.py.

Files: po/ja.po
Copyright: 2009 Red Hat Inc. <ssato@redhat.com>
License: GPL-2
Comment: Japanese message translations for tuna
 Copyright (C) 2009 THE tuna'S COPYRIGHT HOLDER
 This file is distributed under the same license as the tuna package.
 Satoru Satoh <ssato@redhat.com>, 2009.

Files: po/pt_BR.po
Copyright: 2009 Red Hat Inc. <acme@redhat.com>
License: GPL-2
Comment: tuna translation to brazilian portuguese.
 This file is distributed under the same license as the tuna package.
 Arnaldo Carvalho de Melo <acme@redhat.com>, 2009.
 Revision: Luis Claudio R. Goncalves <lgoncalv@redhat.com>

Files: po/zh_CN.po
Copyright: 2009 Red Hat Inc. <jlau@redhat.com>
License: GPL-2
Comment: Simplified Chinese message translations for tuna
 Copyright (C) 2009 THE tuna'S COPYRIGHT HOLDER
 This file is distributed under the same license as the tuna package.
 John Lau <jlau@redhat.com>, 2009.

License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2.1
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation;
 version 2.1 of the License.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy fo the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
 USA
