#!/usr/bin/python3
import os
import sysconfig
from os.path import isfile, relpath
from setuptools import setup

if isfile("MANIFEST"):
    os.unlink("MANIFEST")

SCHEME = 'rpm_prefix'
if SCHEME not in sysconfig.get_scheme_names():
    SCHEME = 'posix_prefix'

# Get PYTHONLIB with no prefix so --prefix installs work.
PYTHONLIB = relpath(sysconfig.get_path('platlib', SCHEME), '/usr')

setup(name="tuna",
      version = "0.19",
      description = "Application tuning GUI",
      author = "Arnaldo Carvalho de Melo",
      author_email = "acme@redhat.com",
      url = "http://userweb.kernel.org/tuna",
      license = "GPLv2",
      long_description =
"""\
Provides interface for changing scheduler and IRQ tunables, at whole CPU and at per
thread/IRQ level. Allows isolating CPUs for use by a specific application and moving
threads and interrupts to a CPU by just dragging and dropping them.
""",
      packages = ["tuna", "tuna/gui"],
)
